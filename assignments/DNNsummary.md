# DEEP NEURAL NETWORKS
Neural network
- A neural network is a network or circuit of neurons, or in a modern sense, an artificial neural network, composed of artificial neurons or nodes.<br>

Deep Neural network
- A deep neural network is a neural network with a certain level of complexity, a neural network with more than two layers. 
- Deep neural networks use sophisticated mathematical modeling to process data in complex ways.

#### "Non-deep" feedforward neural network:
<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/10.JPG" width="300" height="300">

#### Deep neural network:
<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/11.JPG" width="700" height="300">

- The input is fed to the input layer, the neurons perform a linear transformation on this input using the weights and biases.<br>
 ***x = (weight * input) + bias***
 
- Post that, an activation function is applied on the above result.<br>
 ***Y=Activation(Σ (weight * input) + bias)***
 
- Finally, the output from the activation function moves to the next hidden layer and the same process is repeated.<br> 
This forward movement of information is known as the **forward propagation**.

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/16.JPG" width="500" height="400">
 
- Using the output from the forward propagation, error is calculated. Based on this error value, the weights and biases of the neurons are updated.<br> This process is known as **back-propagation**.

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/17.JPG" width="700">
<br>

### Types of Deep Learning Networks:

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/12.JPG" width="700">

<br>

### Activation Functions

**Sigmoid**
- It is one of the most widely used non-linear activation function. 
- Sigmoid transforms the values between the range 0 and 1. 
- Here is the mathematical expression for sigmoid-
***f(x) = 1/(1+e^-x)***

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/13.JPG" width="300">

**ReLU**
- The ReLU function is another non-linear activation function that has gained popularity in the deep learning domain. 
- ReLU stands for Rectified Linear Unit. 
- The main advantage of using the ReLU function over other activation functions is that it does not activate all the neurons at the same time.
- This means that the neurons will only be deactivated if the output of the linear transformation is less than 0. 
***f(x)=max(0,x)***

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/14.JPG" width="300">

### Cost Function
- It is a function that measures the performance of a Machine Learning model for given data. 
- Cost Function quantifies the error between predicted values and expected values and presents it in the form of a single real number
- The cost function minimizes the value of the funtion

### Gradient Descent
- Gradient of the value gives the direction of steepest ascent.
- Gradient descent is an efficient optimization algorithm that attempts to find a local or global minima of a function.
- Gradient descent enables a model to learn the gradient or direction that the model should take in order to reduce errors.

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/15.JPG" width="300">

