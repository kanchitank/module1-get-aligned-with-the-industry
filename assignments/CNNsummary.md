# CONVOLUTIONAL NEURAL NETWORK (CNN)

A **Convolutional Neural Network** (ConvNet/CNN) is a Deep Learning algorithm which can take in an input image, assign importance (learnable weights and biases) to various aspects/objects in the image and be able to differentiate one from the other.

### Primary Ideas behind Convolutional Neural Networks:
- Let the Neural Network learn which kernels are most useful
- Use same set of kernels across entire image (translation invariance)
- Reduces number of parameters and “variance” (from bias-variance point of view)

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/1.JPG" width="700">

### Convolution Settings — Grid Size
Grid Size (Height and Width):
- The number of pixels a kernel “sees” at once
- Typically use odd numbers so that there is a “center” pixel
- Kernel does not need to be square

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/2.JPG" width="700">

### Convolution Settings — Padding
Padding
- Using Kernels directly, there will be an “edge effect”
- Pixels near the edge will not be used as “center pixels” since there are not enough surrounding pixels
- Padding adds extra pixels around the frame
- So every pixel of the original image will be a center pixel as the kernel moves across the image
- Added pixels are typically of value zero (zero-padding)

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/3.JPG" width="700">

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/4.JPG" width="700">

### Convolution Settings — Stride
- The ”step size” as the kernel moves across the image
- Can be different for vertical and horizontal steps (but usually is the same value)
- When stride is greater than 1, it scales down the output dimension

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/5.JPG" width="700">

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/6.JPG" width="700">

### Convolutional Settings — Depth
- In images, we often have multiple numbers associated with each pixel location.
- These numbers are referred to as “channels”<br>
 – RGB image—3 channels<br>
 – CMYK—4 channels<br>
- The number of channels is referred to as the “depth”
- So the kernel itself will have a “depth” the same size as the number of input channels
- Example: a 5x5 kernel on an RGB image<br>
 – There will be 5x5x3 = 75 weights
- The output from the layer will also have a depth
- The networks typically train many different kernels
- Each kernel outputs a single number at each pixel location
- So if there are 10 kernels in a layer, the output of that layer will have depth 10.

### Pooling
- Idea: Reduce the image size by mapping a patch of pixels to a single value.
- Shrinks the dimensions of the image.
- Does not have parameters, though there are different types of pooling operations.

### Pooling: Max-pool
- For each distinct patch, represent it by the maximum
- 2x2 maxpool shown below

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/7.JPG" width="700">

### Pooling: Average-pool
- For each distinct patch, represent it by the average
- 2x2 avgpool shown below

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/8.JPG" width="700">

### Activation Functions
- The activation function is a mathematical “gate” in between the input feeding the current neuron and its output going to the next layer. 
- It can be as simple as a step function that turns the neuron output on and off, depending on a rule or threshold. 
- Or it can be a transformation that maps the input signals into output signals that are needed for the neural network to function.

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/9.JPG" width="700">