# Product 1

### Product name

Face Track

### Product link

[Learn about Face Track](https://visagetechnologies.com/facetrack)

### Product Short description

Face Track detects and tracks one or more faces and their facial features in images and videos from any standard camera or video file in color, gray-scale and near-infrared. For each detected face it returns detailed face data including:

* 2D and 3D head pose and facial points coordinates (chin tip, nose tip, lip corners etc.)
* A set of action units describing the current facial expressions (e.g. jaw drop)
* Eye closure and eye-gaze information
* 3D triangle mesh model of the face in the current pose and expression

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/18.JPG" width="300">

### Product is combination of features

* Emotion Detection
* Pose Estimation

### Product is provided by which company?

Visage Technologies

<br><br><br>

# Product 2

### Product name

Google Photos

### Product link

[Learn about Google Photos](https://www.google.com/photos/about/)

### Product Short description

* Google Photos will automatically arrange your uploaded pictures by location and by date taken. 
* Using advanced image recognition and Google’s large database of information, it can recognize the subject of your photos quite easily.
* Google Photos can detect a person's face and then tags that person for each of their photos.
* Google Photos creates models of the faces in your photos in order to group similar faces together. 

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/19.JPG" width="300">

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/20.JPG" width="300">

### Product is combination of features

* Person Detection
* ID Recognition

### Product is provided by which company?

Google

<br><br><br>

# Product 3

### Product name

PoseMatch2

### Product link

[Learn about PoseMatch2](https://play.google.com/store/apps/details?id=com.gil.posematchv3&hl=en_IN)

### Product Short description

Human Pose Matching demo.<br>
Using Human pose estimation and object detection.<br> 
A nice example of Deep Learning in the field of Computer Vision.<br>
Actions that it can detect are:
* sitting,
* standing,
* raising hand,etc.

<img src = "https://gitlab.com/kanchitank/module1-get-aligned-with-the-industry/-/raw/master/images/21.webp" height="350">

### Product is combination of features

* Pose Estimation

### Product is provided by which company?

BilGeckers
